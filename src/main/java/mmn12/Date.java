package mmn12;

/**
 * matala 12 - using a class to represent a date
 *
 * @author or prager
 */
public class Date {

    private int _day;
    private int _month;
    private int _year;

    private final int DEFAULT_DAY = 1;
    private final int DEFAULT_MONTH = 1;
    private final int DEFAULT_YEAR = 2000;

    private final static int MAX_VALID_MONTH = 12;
    private final static int MIN_VALID_MONTH = 1;
    private final static int MIN_VALID_DAY_IN_MONTH = 1;
    private final static int MAX_VALID_DAY_IN_MONTH = 31;
    private final static int MAX_VALID_YEAR = 9999;
    private final static int MIN_VALID_YEAR = 1000;

    private final static int februaryMonth = 2;  // we can use ENUM to represent months
    private final static int marchMonth = 3;  // we can use ENUM to represent months
    private final static int aprilMonth = 4;
    private final static int juneMonth = 6;
    private final static int septemberMonth = 9;
    private final static int novemberMonth = 11;

    private final int monthsInYear = 12;
    private final int daysInWeek = 7;

    /**
     * creates a new date object
     * @param day the day in the month(1-31)
     * @param month the month in the year
     * @param year the year (in 4 digits)
     */
    public Date(int day, int month, int year) {

        if (Date.isValidDate(day, month, year)) {
            this._day = day;
            this._month = month;
            this._year = year;
        } else {
            this._day = DEFAULT_DAY;
            this._month = DEFAULT_MONTH;
            this._year = DEFAULT_YEAR;
        }

    }

    /**
     * copy constructor
     * @param other other date to copy
     */
    public Date(Date other) {
        this._day = other.getDay();
        this._month = other.getMonth();
        this._year = other.getYear();
    }

    /**
     * checking if the received date is valid
     * @param day the day in the month(1-31)
     * @param month the month in the year
     * @param year the year (in 4 digits)
     * @return true if it's valid date, otherwise false.
     */
    private static boolean isValidDate(int day, int month, int year) {
        // If year, month and day are not in given range
        if (year < MIN_VALID_YEAR || year > MAX_VALID_YEAR)
            return false;

        if (month < MIN_VALID_MONTH || month > MAX_VALID_MONTH)
            return false;

        if (day < MIN_VALID_DAY_IN_MONTH || day > MAX_VALID_DAY_IN_MONTH)
            return false;

        // Handle February month with leap year
        if (month == februaryMonth) {
            if (leap(year))
                return (day <= 29);
            else
                return (day <= 28);
        }

        // Months of April, June, Sep and Nov must have number of days less than or equal to 30.
        if (month == aprilMonth || month == juneMonth || month == septemberMonth || month == novemberMonth)
            return (day <= 30);

        return true;
    }

    /**
     * Checking if the received year is leap
     * @param year year to check
     * @return true if it's leap year, otherwise false.
     */
    private static boolean leap (int year)
    {
        return (year % 4 == 0 && year % 100 != 0) || (year % 400 == 0);
    }

    /**
     * Check if another received date is the same as this date
     * @param other other date to compare
     * @return true if they equal, otherwise false
     */
    public boolean equals(Date other) {
        if (this == other)
            return true;

        if (other == null)
            return false;

        return _day == other.getDay() &&
                _month == other.getMonth() &&
                _year == other.getYear();
    }

    /**
     * check if a received date is before this date
     * @param other other date to compare
     * @return true if the received date is before this date, otherwise false.
     */
    public boolean before(Date other) {
        if (this._year == other.getYear()) {
            // means that they both in the same year
            if (this._month == other.getMonth()) {
                // means that they both in the same month
                return this._day < other.getDay();
            }
            else
                return this._month < other.getMonth();
            }

        return this._year < other.getYear();
    }

    /**
     * check if a received date is after this date
     * @param other other date to compare
     * @return true if the received date if after this date, otherwise false
     */
    public boolean after(Date other) {
        return !this.before(other);
    }

    /**
     * return the difference in days between received date and this date
     * @param other other date to compare
     * @return the number of days between the dates
     */
    public int difference(Date other) {
        return Math.abs(calculateDate(this.getDay(), this.getMonth(), this.getYear()) - calculateDate(other.getDay(), other.getMonth(), other.getYear()));
    }

    /**
     * computes the day number since the beginning of the Christian counting of years
     * @param day the day in the month(1-31)
     * @param month the month in the year
     * @param year the year (in 4 digits)
     * @return the number of days since the begining of the christian counting of years
     */
    private int calculateDate ( int day, int month, int year)
    {
        if (month < marchMonth) {
            year--;
            month = month + monthsInYear;
        }
        return 365 * year + year/4 - year/100 + year/400 + ((month+1) * 306)/10 + (day - 62);
    }

    /**
     * @return string representation of the date
     */
    @Override
    public String toString() {
        return this.getDay() + "/" + this.getMonth() + "/" + this.getYear();
    }

    /**
     * @return return the day in week of this date
     */
    public int dayInWeek() {
        int year = this.getYear();

        int month = this.getMonth();
        if (month < marchMonth) {
            year--;
            month += monthsInYear;
        }

        int C = year / 100;  // C for century, e.g: 1903 -> 19
        year = year % 100;      // Y e.g: 1903 -> 03 -> 3

        int dayInWeek = ((this.getDay() + (26 * (month + 1))/10 + year + year/4 + C/4 - 2 * C) % 7);
        if (dayInWeek < 0)
            // Sometimes dayInWeek can be negative so we normalize it for scale of 7 days in a week.
            dayInWeek = Math.floorMod (dayInWeek, daysInWeek);

        return dayInWeek;
    }

    /** sets date's day */
    public void setDay(int dayToSet) {
        if (Date.isValidDate(dayToSet, _month, _year)) {
            this._day = dayToSet;
        }
    }

    /** sets date's month */
    public void setMonth(int monthToSet) {
        if (Date.isValidDate(_day, monthToSet, _year)) {
            this._month = monthToSet;
        }
    }

    /** sets date's year */
    public void setYear(int yearToSet) {
        if (Date.isValidDate(_day, _month, yearToSet)) {
            this._year = yearToSet;
        }
    }

    /** gets date's day */
    public int getDay() {
        return _day;
    }

    /** gets date's month */
    public int getMonth() {
        return _month;
    }

    /** gets date's year */
    public int getYear() {
        return _year;
    }
}

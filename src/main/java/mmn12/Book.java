package mmn12;

/**
 * matala 12 - using a class to represent a borrowable book
 *
 * @author or prager
 */
public class Book {
    private final int MAX_DAYS = 30;
    private final int DOUBLE_FINE_MULTIPLIER = 2;
    private final int PENALTY = 5;

    private final int MIN_VALID_PUB_YEAR = 1800;
    private final int MAX_VALID_PUB_YEAR = 2018;

    private final int DEFAULT_PUB_YEAR = 2000;
    private final int DEFAULT_NUMBER_OF_PAGES = 1;

    private String _title;
    private String _author;
    private int _yearPublished;
    private int _noOfPages;
    private boolean _borrowed;
    private String _studentName;
    private Date _borrowedDate;
    private Date _returnDate;

    private final int fridayDayInWeek = 5;
    private final int saturdayDayInWeek = 6;

    /**
     * create a new book object
     * @param title book's title (name)
     * @param author book's author name
     * @param year book's publish year
     * @param num number of pages
     */
    public Book(String title, String author, int year, int num)
    {
        _title = title;
        _author = author;

        if (isYearPublishValid(year)) {
            _yearPublished = year;
        } else {
            _yearPublished = DEFAULT_PUB_YEAR;
        }

        if (isValidNumberOfPages(num)) {
            _noOfPages = num;
        } else {
            _noOfPages = DEFAULT_NUMBER_OF_PAGES;
        }

        _borrowed = false;
    }

    /**
     * copy constructor
     * @param other other to be copied
     */
    public Book(Book other) {
        // todo: that's the way to do copy contractor?
        _title = other.getTitle();
        _author = other.getAuthor();

        if (isYearPublishValid(other.getYear())) {
            _yearPublished = other.getYear();
        } else {
            _yearPublished = DEFAULT_PUB_YEAR;
        }

        if (isValidNumberOfPages(other.getPages())) {
            _noOfPages = other.getPages();
        } else {
            _noOfPages = DEFAULT_NUMBER_OF_PAGES;
        }

        _borrowed = false;
    }

    /**
     * borrow the book if it's now already borrowed
     * @param name the name of the student that wants to borrow the book
     * @param d the date of borrowing
     */
    public void borrowBook(java.lang.String name, Date d) {
        if (!this.getBorrowed()) {
            this._studentName = name;
            this._borrowedDate = d;
            _borrowed = true;
        }
    }

    /**
     * calculate the penalty value
     * @param d the book's return date
     * @return the penalty required to be paid for late return / 0 in case of early return
     */
    public int computePenalty(Date d) {
        int daysDelta = d.difference(this.getBorrowedDate());

        if (this.getBorrowed() && d.after(this.getBorrowedDate()) && daysDelta > MAX_DAYS) {
            int daysToPayFineFor = daysDelta - MAX_DAYS;
            if (daysToPayFineFor > MAX_DAYS) {
                return (PENALTY * MAX_DAYS) + PENALTY * DOUBLE_FINE_MULTIPLIER * (daysDelta - MAX_DAYS);
            } else {
                return PENALTY * (daysToPayFineFor);
            }
        } else {
            return 0;
        }
    }

    /** gets the author name */
    public String getAuthor() {
        return _author;
    }

    /** return true if the book is borrowed, otherwise - false */
    public boolean getBorrowed() {
        return _borrowed;
    }

    /** gets borrowed date */
    public Date getBorrowedDate() {
        return _borrowedDate;
    }

    /** gets book's number of pages */
    public int getPages() {
        return _noOfPages;
    }

    /** gets return date */
    public Date getReturnDate() {
        return _returnDate;
    }

    /** gets borrowing student name */
    public String getStudentName() {
        return _studentName;
    }

    /** gets book's title */
    public String getTitle() {
        return _title;
    }

    /** gets book's publish year */
    public int getYear() {
        return _yearPublished;
    }

    /**
     * Calculate the number of days the book borrowed
     * @param d a date to compare to (e.g: can be today)
     * @return the number of days between borrowing day to the received date
     */
    public int howLongBorrowed(Date d) {
        if (this.getBorrowed() && d.after(this.getBorrowedDate())) {
            return d.difference(this.getBorrowedDate());
        } else {
            return 0;
        }
    }

    /**
     * Checking if the book is available for borrowing
     * @param d a date to borrow the book at
     * @return true if it's available at the received date, otherwise - false.
     */
    public boolean isAvailable(Date d){
        if (this.getBorrowed()) {
            return false;
        } else {
            if (d.dayInWeek() == fridayDayInWeek || d.dayInWeek() == saturdayDayInWeek) {
                return false;
            } else {
                return true;
            }
        }
    }

    /**
     * Check if another (received) book is older than this book
     * @param other other book to comopare it's publish year
     * @return true if newer, otherwise false.
     */
    public boolean olderBook(Book other) {
        return other.getYear() > this.getYear();
    }

    /**
     * return the book
     * @param d a date to return the book at
     * @return true if it's not borrowed or returned at time, in case of late return return false.
     */
    public boolean returnBook(Date d) {

        if (this.getBorrowed()) {
            this._returnDate = d;

            boolean isReturnLate = d.difference(_borrowedDate) > MAX_DAYS;

            this._borrowed = false;
            this._borrowedDate = null;
            this._studentName = null;

            return isReturnLate;

        } else {
            return true;
        }
    }

    /**
     * check if this book has the same author as the received book
     * @param other other book to compare it's author
     * @return true if it's the same author, otherwise false.
     */
    public boolean sameAuthor(Book other) {
        return other.getAuthor().equals(this.getAuthor());
    }

    /**
     * sets the author name
     * @param author author name to set
     */
    public void setAuthor(String author) {
        this._author = author;
    }

    /**
     * sets book's number of pages
     * @param n number of pages to set
     */
    public void setPages(int n) {
        if (isValidNumberOfPages(n)) {
            this._noOfPages = n;
        }
    }

    /**
     * sets book's title
     * @param title title to set for the book
     */
    public void setTitle(String title) {
        this._title = title;
    }

    /**
     * sets book's publish year
     * @param year year of publish to set
     */
    public void setYear(int year) {
        if (isYearPublishValid(year)) {
            this._yearPublished = year;
        }
    }

    /**
     * Check if a specific year is a valid year of publish
     * @param year year to check it's validity
     * @return true if valid, otherwise false
     */
    private boolean isYearPublishValid(int year) {
        return (year >= MIN_VALID_PUB_YEAR && year <= MAX_VALID_PUB_YEAR);
    }

    /**
     * check if the received number of pages is with valid value
     * @param numberOfPages  number of pages to check it's validity
     * @return true if valid, otherwise false.
     */
    private boolean isValidNumberOfPages(int numberOfPages) {
        return numberOfPages > 0;
    }

    /**
     * @return string that represent the book
     */
    public String toString() {
        return "Title: " + this.getTitle() + "\t" + "Author: " + this.getAuthor() + "\t" + "Year: " + this.getYear() +
                ", " + this.getPages() + " pages";
    }

    /**
     * Checking if another received book is the same at this book
     * @param o another book for comparison
     * @return true if they equals, otherwise false.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Book book = (Book) o;
        return _yearPublished == book._yearPublished &&
                _noOfPages == book._noOfPages &&
                _title.equals(book._title) &&
                _author.equals(book._author);
    }

}

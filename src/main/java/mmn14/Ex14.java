package mmn14;

/**
 * matala 14
 *
 * @author or prager
 */
public class Ex14 {

    /**
     * answer to question 1 a :
     * the true statements are : 3, 5 and 6
     */

    private static final int ELEVATOR_FLOOR = 20;
    private static final int ONE_STEP = 1;
    private static final int TWO_STEPS = 2;
    private static final int MAX_FLOORS = 30;
    private static final int MIN_FLOORS = 1;
    private static final int GROUND_FLOOR = 0;

    public static boolean findValWhat(int[][] m, int val) {
        /**
         * the method checks if val exists in 2d array m,
         * time complexity: O(n)
         * memory complexity: O(n)
         *
         *
         * @param m: 2d (n x n) matrix to search the value in
         * @param val: the value to look for in matrix m
         * @return: true if the value val exists in matrix m, otherwise false.
         */
        int row = 0;
        int col = m.length - 1;

        while (row < m.length && col >= 0) {
            if (m[row][col] == val) {
                return true;
            }
            if (m[row][col] > val)
                // means that the value is greater the our requested value, therefor it might be in it's column
                col--;
            else {
                // means than the current value is much lower than our requested value, therefor it not going to be
                // located in it's row, and we need to look in the next row.
                row++;
            }
        }

        return false;
    }

    public static boolean findValTest(int [][]m, int val) {
        /**
         * the method check if the value val exists in the 2d matrix m
         * time complexity: O(n)
         * memory complexity: O(n)
         *
         * @param m: 2d (n x n) matrix to search the value in
         * @param val: the value to look for in matrix m
         * @return: true if the value val exists in matrix m, otherwise false.
         */
        for (int row=m.length - 1; row >= 0; row--) {
            if (m[row][0] == val) {
                return true;
            } else {
                if (m[row][0] < val) {
                    // means that there is no chance that the value in rows before
                    for (int nextRow=0; (nextRow <=1) && (nextRow + row < m.length); nextRow++) {
                        // the value can be in current row or the next one, so we'll only iterate on the 2 of them.
                        for (int col=1; col < m.length; col++) {
                            if (m[row + nextRow][col] == val) {
                                return true;
                            }
                        }
                    }
                    return false;
                }
            }
        }
        return false;
    }

    public static int subStrC(String s, char c) {
        /**
         * the method return the number of sub strings started, ended and including only one occurrence of char c.
         *
         * @param s: the string to look for substrings in
         * @param c: the char that starts, ends and should be inside the substrings
         * @return: the amount of valid substring in string s
         */
        int matchedSubStrings = 0;
        int charIndex = 0;

        int subStartedIndex = -1;
        int subMiddleFoundIndex = -1;

        while (charIndex < s.length()) {
            if (s.charAt(charIndex) == c) {
                // means that it could be the start / middle of end char
                if (subStartedIndex == -1) {
                    // means that it's the first c of the possible valid substring
                    subStartedIndex = charIndex;
                } else if (subMiddleFoundIndex == -1) {
                    // means that the sub string started and we find the requested middle char
                    subMiddleFoundIndex = charIndex;
                } else {
                    // means that it's the last requested char for our valid substring
                    matchedSubStrings ++;

                    subStartedIndex = -1;
                    charIndex = subMiddleFoundIndex - 1;
                    subMiddleFoundIndex = - 1;

                }
            }
            charIndex ++;
        }

        return matchedSubStrings;
    }

    public static int subStrMaxC(String s, char c, int k) {
        /**
         * the method count the amount of substring with k max occurrences of char c inside substring
         *
         * @param s: the string to look for substrings in
         * @param c: the char that starts, ends and should be inside the substrings
         * @param k: the max amount of time char c can be inside substring
         * @return the amount of valid substrings
         */

        int validSubStrings = 0;
        int cCharCount = 0;

        for (int i = 0; i < s.length(); i++) {
            // we count the amount of occurrences of char c in string s
            if (s.charAt(i) == c) {
                cCharCount ++;
            }
        }

        for (int j = 0; (j <= k) && (j < cCharCount); j++) {
            // count the amount of valid substring from the position of single c char
            int tmpValidSubStrings = cCharCount - j - 1;

            if (tmpValidSubStrings == 0) {
                break;
            } else {
                validSubStrings += tmpValidSubStrings;
            }
        }

        return validSubStrings;
    }

    public static int spiderman(int n) {
        /**
         * method than calc the amount of routes that spiderman can take in order to reach the n floor
         */
        if (n < MIN_FLOORS) {
            return 0;
        }

        return stepsFromCurrentFloor(n, GROUND_FLOOR);
    }

    private static int stepsFromCurrentFloor(int numberOfFloors, int currentFloor) {
        /**
         * recursive method that calc the amount of steps from specific (currentFloor) floor to the n (numberOfFloors)
         * floor.
         *
         * @param numberOfFloors the floor we would like to calc the possible routes to
         * @param currentFloor the floor which we would like to calc the routes from
         * @return: the amount of possible routes to the top floor
         *
         */
        if (numberOfFloors == currentFloor) {
            // means that we reached the last floor
            return ONE_STEP;
        } else if (currentFloor > numberOfFloors) {
            return 0;
        }

        return stepsFromCurrentFloor(numberOfFloors, currentFloor + ONE_STEP) +
                stepsFromCurrentFloor(numberOfFloors, currentFloor + TWO_STEPS);

    }

    public static int spidermanPhoneBooth20(int n) {
        /**
         * method that calc the possible routes from ground floor to n floor including using 20'th floor elevator
         *
         * @param n: the amount of floors
         * @return: the amount of possible routes to the n floor
         */
        if (n < ELEVATOR_FLOOR || n >= MAX_FLOORS) {
            return 0;
        }

        return stepsFromCurrentFloorBooth20(n, GROUND_FLOOR);
    }

    private static int stepsFromCurrentFloorBooth20(int topFloor, int currentFloor) {
        /**
         * recursive method that calc the steps from current floor to top floor
         *
         * @param topFloor: destination floor
         * @param currentFloor: the floor to calc step to destination from
         * @return: the amount of possible routes from current floor to top floor
         */
        if (currentFloor == ELEVATOR_FLOOR || currentFloor == topFloor) {
            return ONE_STEP;
        } else if (currentFloor > topFloor) {
            return 0;
        } else {
            return stepsFromCurrentFloorBooth20(topFloor, currentFloor + ONE_STEP) +
                    stepsFromCurrentFloorBooth20(topFloor, currentFloor + TWO_STEPS);
        }
    }

    public static int countPaths(int[][] mat) {
        /**
         * the method counts the possible paths from starting location 0,0 to the last cell
         *
         * @param mat: n x m matrix to look for possible routes in
         * @return: the amount of possible routes
         */
        int numberOfRows = mat.length - 1;
        int numberOfColumns = mat[0].length - 1;

        return countPathsFromLocation(mat, 0, 0, numberOfRows, numberOfColumns);


    }

    private static int countPathsFromLocation(int[][] mat, int rowNum, int colNum, int lastRow, int lastCol) {
        /**
         * recursive method for calculating the amount of valid paths from a location to destination in a n x m matrix
         * @param mat: matrix to look for routes in
         * @param rowNum: current row number
         * @param colNum: current column number
         * @param lastRow: the last row, used for detecting last cell (destination)
         * @param lastCol: the last column, used for detecting last cell (destination)
         * @return: the amount of possible routes to destination
         */
        if (rowNum == lastRow && colNum == lastCol) {
            // means that we reached the last cell
            return 1;
        } else if (rowNum > lastRow || colNum > lastCol) {
            return 0;
        } else {
            int nextOptionNum1Row = rowNum + (mat[rowNum][colNum] / 10);
            int nextOptionNum1Col = colNum + (mat[rowNum][colNum] % 10);

            int nextOptionNum2Row = rowNum + (mat[rowNum][colNum] % 10);
            int nextOptionNum2Col = colNum + (mat[rowNum][colNum] / 10);

            if (nextOptionNum1Row != nextOptionNum2Row && nextOptionNum1Col != nextOptionNum2Col) {
                // means that there are two diff options
                return countPathsFromLocation(mat, nextOptionNum1Row, nextOptionNum1Col, lastRow, lastCol) +
                        countPathsFromLocation(mat, nextOptionNum2Row, nextOptionNum2Col, lastRow, lastCol);
            } else {
                // means that there is only one option for the next step
                if (nextOptionNum1Row == rowNum && nextOptionNum1Col == colNum) {
                    // means that we are on 0 which means end without reaching destination
                    return 0;
                }
                return countPathsFromLocation(mat, nextOptionNum1Row, nextOptionNum1Col, lastRow, lastCol);
            }

        }
    }
}

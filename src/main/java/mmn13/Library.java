package mmn13;

/**
 * matala 13 - using a class to represent a library
 *
 * @author or prager
 */
public class Library {

    private final int MAX_BOOKS = 200;
    private int _noOfBooks;
    private Book[] _lib;

    /**
     * Creates a new library object
     */
    public Library() {
        this._lib = new Book[MAX_BOOKS];
        this._noOfBooks = 0;
    }

    /**
     * Add new book to the library
     * @param b the book to add to the library
     * @return true if added successfully, otherwise false
     */
    public boolean addBook(Book b) {
        if (_noOfBooks < MAX_BOOKS) {
            this._lib[_noOfBooks] = b;
            _noOfBooks += 1;
            return true;
        } else {
            return false;
        }
    }

    /**
     * Remove book from the library
     * @param b book to remove from the library
     */
    public void removeBook(Book b) {

        Book[] tmpLib = new Book[MAX_BOOKS]; // temp book array that will contain only the valid books to remain in _lib
        int j = 0; // Counter for the tmp book array

        for (int i = 0; i < _noOfBooks; i++) {
            // for each book in library
            Book bookInLibrary = _lib[i];

            if (!bookInLibrary.equals(b)) {
                // Means that the book we iterating on is not the book we looking for to remove
                tmpLib[j] = bookInLibrary;
                j ++;
            }
        }

        _lib = tmpLib;
        _noOfBooks = j;

    }

    /**
     * Return the amount of borrowed books in the library
     * @return Return the amount of borrowed books in the library
     */
    public int howManyBooksBorrowed () {

        int borrowedBooksCounter = 0; // Counter for the books that already been borrowed

        for (int i=0; i < _noOfBooks; i++) {
            Book bookInLibrary = _lib[i];
            if (bookInLibrary.getBorrowed()) {
                // Means that the book is borrowed
                borrowedBooksCounter ++;
            }
        }

        return borrowedBooksCounter;
    }

    /**
     * Return the amount of borrowed books by specific student by its name
     * @param name the name of the student
     * @return the amount of books borrowed by the student
     */
    public int howManyBooksBorrowedToStudent (String name) {
        int booksBorrowedForStudent = 0; // Counter for the books that already been borrowed

        for (int i=0; i < _noOfBooks; i++) {
            Book bookInLibrary = _lib[i];
            if (bookInLibrary.getStudentName() != null && bookInLibrary.getStudentName().equals(name)) {
                // Means that the book is borrowed by the student
                booksBorrowedForStudent ++;
            }
        }

        return booksBorrowedForStudent;

    }

    /**
     * Return the most popular author among the books in the library
     * @return Return the most popular author among the books in the library
     */
    public String mostPopularAuthor() {
        if (_noOfBooks == 0) {
            return null;
        } else {

            String mostPopularAuthor = _lib[0].getAuthor();
            int mostPopularAuthorBookCount = 0;

            for (int i=0; i < _noOfBooks; i++) {

                Book bookInLibrary = _lib[i];

                int authorBookCount = 1;
                if (bookInLibrary.getAuthor() != null) {
                    for (int j=i; j < _noOfBooks; j++) {

                        Book otherBook = _lib[j];

                        if (bookInLibrary == otherBook) {
                            // means that it's the same book object
                            continue;
                        }

                            if (otherBook.getAuthor() != null && otherBook.getAuthor().equals(bookInLibrary.getAuthor())) {
                            authorBookCount++;
                        }
                    }

                    if (authorBookCount > mostPopularAuthorBookCount) {
                        // means that the current author is much more popular than the last most popular one
                        mostPopularAuthor = bookInLibrary.getAuthor();
                        mostPopularAuthorBookCount = authorBookCount;
                    }
                }


            }

            return mostPopularAuthor;
        }
    }

    /**
     * Return the oldest book in the library
     * @return Return the oldest book in the library
     */
    public Book oldestBook () {
        if (_noOfBooks == 0) {
            return null;
        } else {
            Book oldestBook = _lib[0];

            for (int i = 1; i < _noOfBooks; i++) {
                Book bookInLibrary = _lib[i];
                if (bookInLibrary.getYear() < oldestBook.getYear()) {
                    // means that current book's publish year is earlier than the last oldest book checked
                    oldestBook = bookInLibrary;
                }
            }

            return oldestBook;
        }
    }

    /**
     * Remove single instance of book from the library by it's name
     * @param name the name of the book to remove
     * @return the removed book
     */
    public Book remove(String name) {

        Book[] tmpLib = new Book[MAX_BOOKS];
        int tmpLibBookCnt = 0;
        Boolean removed = false;
        Book removedBook = null;

        for (int i=0; i < _noOfBooks; i++) {
            Book bookInLibrary = _lib[i];
            if (bookInLibrary.getTitle().equals(name) && !removed) {
                removedBook = bookInLibrary;
                removed = true;
            } else {
                tmpLib[tmpLibBookCnt] = bookInLibrary;
                tmpLibBookCnt ++;
            }
        }

        _lib = tmpLib;
        _noOfBooks = tmpLibBookCnt;

         return removedBook;

    }

    /**
     * returns representation of the library as string
     * @return representation of the library as string
     */
    public String toString() {

        //"The books in the library are: Title: Pride and Prejudice    Author: Jane Austen Year: 1813, 350 pages"
        String libraryAsString = "The books in the Library are:\n";

        for (int i=0; i < _noOfBooks; i++) {
            Book bookInLibrary = _lib[i];
            libraryAsString += bookInLibrary.toString() + "\n";
        }

        return libraryAsString;
    }
}

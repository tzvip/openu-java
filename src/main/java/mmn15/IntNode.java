package mmn15;

public class IntNode {
    private int _value;
    private IntNode _next;

    public IntNode(int value, IntNode next) {
        this._value = value;
        this._next = next;
    }

    public void setNext(IntNode next)  {
        this._next = next;
    }

    public int getValue() {
        return this._value;
    }

    public IntNode getNext() {
        return this._next;
    }
}

package mmn15;

/**
 * matala 15
 *
 * @author or prager
 */

/**
 * Class representing big number and some big number methods.
 */
public class BigNumber {
    /**
     * head is a node pointing to the units number, the next of node points to the next digit until the number's last
     * digit.
     */
    private IntNode head;

    /**
     * constructor for the number zero.
     * space complexity is O(1)
     * time complexity is O(1)
     */
    public BigNumber() {

        head = getZeroNode();
    }

    /**
     * the method return node for the number zero, it's being used when we want to init big number to zero.
     * space complexity is O(1)
     * time complexity is O(1)
     */
    private IntNode getZeroNode() {

        return new IntNode(0, null);
    }

    /**
     *
     *
     * @param num
     * @return
     */
    private Boolean isZeroBigNumber(BigNumber num) {
        return isZeroNode(num.head);
    }

    /**
     *
     * time complexity: O(1)
     * space complexity: O(1)
     *
     * @param node:
     * @return
     */
    private Boolean isZeroNode(IntNode node) {
        return (node.getValue() == 0 && node.getNext() == null);
    }

    /**
     * constructor for building a big number from long number.
     * time complexity is O(n)
     * space complexity is O(n
     *
     * @param num: long number to convert to big number.
     */
    public BigNumber(long num)  {

        if (num == 0) {
            head = getZeroNode();
        } else {
            head = recursiveBuildBigNumber(num);
        }
    }

    /**
     * deep copy constructor, copy other big number to this big number.
     * space complexity is O(n
     * time complexity is O(n)
     */
    public BigNumber(BigNumber bigNumber) {

        head = copyBigNumberNodes(bigNumber.getHead());
    }

    /**
     * recursive method that doing deep copy in order to copy a chain of nodes.
     * space complexity is O(n
     * time complexity is O(n)
     */
    private IntNode recursiveBuildBigNumber(long num) {
        /**
         * recursive method that doing deep copy in order to copy a chain of nodes.
         * space complexity is O(n
         * time complexity is O(n)
         */
        if (num == 0) {
            return null;
        } else {
            int unitNumber = (int) (num % 10);
            return new IntNode(unitNumber, recursiveBuildBigNumber(num / 10));
        }

    }

    /**
     * recursive method that doing deep copy in order to copy a chain of nodes.
     * space complexity is O(n
     * time complexity is O(n)
     */
    private IntNode copyBigNumberNodes(IntNode num) {

        if (num.getNext() == null) {
            return num;
        } else {
            return new IntNode(num.getValue(), copyBigNumberNodes(num.getNext()));
        }
    }

    /**
     * return this big number's unit digit pointer.
     */
    public IntNode getHead() {

        return this.head;
    }

    /**
     * return a string that represent the big number.
     * space complexity is O(n
     * time complexity is O(n)
     */
    public String toString() {

        return recursivePrintNodes(this.head);
    }

    /**
     * recursive method that building a string from all the digits (nodes) in the big number.
     * space complexity is O(n
     * time complexity is O(n)
     * @param intNode: node that represent a digits that should be turned to string
     */
    private String recursivePrintNodes(IntNode intNode) {

        if (intNode.getNext() ==  null) {
            return String.valueOf(intNode.getValue());
        } else {
            return recursivePrintNodes(intNode.getNext()) + String.valueOf(intNode.getValue());
        }
    }

    /**
     * doing this big number plus another big number
     * space complexity is O(n)
     * time complexity is O(n)
     * @param other: another big number to add to this big number
     * @return: a new big number represent the sum of both big numbers.
     */
    public BigNumber addBigNumber(BigNumber other) {

        int sum = this.getHead().getValue() + other.getHead().getValue();
        BigNumber bigNumber;
        int extraPlus = 0;

        if (sum <= 9) {
            bigNumber = new BigNumber(sum);
        } else {
            bigNumber = new BigNumber(sum - 10);
            extraPlus = 1;
        }

        IntNode sumNode = sumNodes(this.getHead().getNext(), other.getHead().getNext(), extraPlus);
        bigNumber.getHead().setNext(sumNode);

        return bigNumber;

    }

    /**
     * sum two nodes values into new one
     * space complexity is O(n
     * time complexity is O(n)
     * @param node1: a node to sum its value
     * @param node2: another node to sum its value
     * @param extraPlus: placeholder for the addition value when the sum value of two parameters is much bigger than
     *                   9.
     * @return: node represent the sum of the two numbers
     */
    private IntNode sumNodes(IntNode node1, IntNode node2, int extraPlus) {

        if (node1 == null && node2 == null && extraPlus == 0) {
            return null;
        } else {
            int node1Value = 0;
            int node2Value = 0;

            if (node1 != null) {
                node1Value = node1.getValue();
            } else {
                node1 = getZeroNode();
            }

            if (node2 != null) {
                node2Value = node2.getValue();
            } else {
                node2 = getZeroNode();
            }

            int sum = node1Value + node2Value + extraPlus;

            if (sum <= 9) {
                return new IntNode(sum, sumNodes(node1.getNext(), node2.getNext(), 0));
            } else {
                return new IntNode(sum - 10, sumNodes(node1.getNext(), node2.getNext(), 1));
            }
        }
    }

    /**
     * the method create a new big number represent the sum of this big number and a long number.
     * space complexity is O(n
     * time complexity is O(n)
     *
     * @param num: num to sum with this big number
     * @return: new big number represent the sum of this big number and another long number.
     */
    public BigNumber addLong(long num) {

        BigNumber longAsBigNumber = new BigNumber(num);

        return this.addBigNumber(longAsBigNumber);
    }

    /**
     * the method create a new big number represent the subtract of this big number and a long number.
     * space complexity is O(n
     * time complexity is O(n)
     *
     * @param other: big number to subtract with this big number
     * @return: new big number represent the subtract  of this big number and another big number.
     */
    public BigNumber subtractBigNumber(BigNumber other) {

        int subtract;
        int extraMinus = 0;
        IntNode subtractNode;
        BigNumber bigNumber;

        int biggerNodeIndex = compareNodes(this.getHead(), other.getHead());
        if (biggerNodeIndex == 0) {
            // means they are equal
            return new BigNumber();
        } else if (biggerNodeIndex == 1) {
            // means that node 1 > node 2
            subtract = this.getHead().getValue() - other.getHead().getValue();
            if (subtract < 0) {
                subtract += 10;
                extraMinus = 1;
            }
            bigNumber = new BigNumber(subtract);

            subtractNode = subtractNodes(this.getHead().getNext(), other.getHead().getNext(), extraMinus);

        } else {
            // means that node 2 > node 1
            subtract = other.getHead().getValue() - this.getHead().getValue();
            if (subtract < 0) {
                subtract += 10;
                extraMinus = 1;
            }
            bigNumber = new BigNumber(subtract);
            subtractNode = subtractNodes(other.getHead().getNext(), this.getHead().getNext(), extraMinus);
        }

        bigNumber.getHead().setNext(subtractNode);
        return bigNumber;
    }

    /**
     * the method subtract two nodes (linked lists representing numbers)
     * space complexity is O(n
     * time complexity is O(n)
     *
     * @param node1: a node to subtract from its value
     * @param node2: another node to subtract its value from the first node
     * @param extraMinus: placeholder for the addition value when the subtract value of two parameters is much lower than
     *                   0.
     * @return: node represent the subtract of the two numbers
     */
    private IntNode subtractNodes(IntNode node1, IntNode node2, int extraMinus) {

        if (node1 == null && node2 == null) {
            return null;
        } else {
            int node1Value = 0;
            int node2Value = 0;

            if (node1 != null) {
                node1Value = node1.getValue();
            } else {
                node1 = getZeroNode();
            }

            if (node2 != null) {
                node2Value = node2.getValue();
            } else {
                node2 = getZeroNode();
            }

            node1Value = node1Value - extraMinus;
            int subtractValue;

            if (node1Value < node2Value) {
                node1Value += 10;
                subtractValue = node1Value - node2Value;
                extraMinus = 1;
            } else {
                subtractValue = node1Value - node2Value;
                extraMinus = 0;
            }

            if (subtractValue == 0 && node1.getNext() == null && node2.getNext() == null) {
                return null;
            } else {
                IntNode nextSubtracted = subtractNodes(node1.getNext(), node2.getNext(), extraMinus);
                if (nextSubtracted == null && subtractValue == 0) {
                    return null;
                }
                return new IntNode(subtractValue, nextSubtracted);
            }
        }
    }

    /**
     * the method comparing two big numbers
     * @param other: another big number to compare with.
     * @return: 1 of this big number is much bigger, -1 if this big number is much lower and 0 if equal.
     */
    public int compareTo(BigNumber other) {
        return compareNodes(this.getHead(), other.getHead());
    }

    /**
     * the method check which number is higher, 1 if the first one, -1 if the second 1 the 0 if equal
     * @param val1: a value to compare
     * @param val2: another value to compare with
     * @return: 1 if the first one is the bigger number, -1 if the second 1 the 0 if equal
     */
    private int getBiggerValueIndex(int val1, int val2) {

        if (val1 > val2) {
            return 1;
        } else if (val2 > val1) {
            return -1;
        } else {
            return 0;
        }
    }

    /**
     * the method compare two linked lists (representing numbers) and returned a code representing the bigger number
     * @param node1: a node to compare its represented number
     * @param node2: another node to compare with
     * @return: 1 if the first one represent the bigger number, -1 if the second 1 the 0 if equal
     */
    private int compareNodes(IntNode node1, IntNode node2) {
        if (node1.getNext() != null && node2.getNext() == null) {
            // means that node 1 > node 2
            return 1;
        } else if (node2.getNext() != null && node1.getNext() == null) {
            // means that node 2 > node 1
            return -1;
        } else if (node1.getNext() == null && node2.getNext() == null) {
            return getBiggerValueIndex(node1.getValue(), node2.getValue());
        } else {
            int biggerNodeIndex = compareNodes(node1.getNext(), node2.getNext());
            if ( biggerNodeIndex == 0) {
                // means that the next iteration equal
                return getBiggerValueIndex(node1.getValue(), node2.getValue());
            } else {
                return biggerNodeIndex;
            }
        }
    }

    /**
     * multiplying one number represented as node with another one's digit
     * time complexity: O(n^2)
     * space complexity: O(n)
     *
     * @param finalResultNode: node that points to the final result which will be set to the big number represent the
     *                       partial result
     * @param multResult: node that store the results of a specific multiplication phase
     * @param smallNumberDigit: digit of the smaller number to multiply the bigger with
     * @param biggerNumberNode: represent the bigger number
     * @return big number represent the partial multiplication result
     */
    private BigNumber multiplyNumberWithANode(IntNode finalResultNode, IntNode multResult, IntNode smallNumberDigit,
                                              IntNode biggerNumberNode) {

        int extraPlus = 0;
        int tempMultiplicationResult;
        int unitsDigit;

        while (biggerNumberNode != null) {

            // multiply bigger number digit with smaller number digit (+extra from previous summing)
            tempMultiplicationResult = (biggerNumberNode.getValue() * smallNumberDigit.getValue()) + extraPlus;

            // extract unit and extra plus digits from the result
            unitsDigit = tempMultiplicationResult % 10;
            extraPlus = tempMultiplicationResult / 10;

            if (multResult != null) {
                multResult.setNext(new IntNode(unitsDigit, null));
                multResult = multResult.getNext();
            } else {
                // thats the first time we multiply our digit with the other number and we want to move forward our
                // result container by keep pointer to the head
                multResult = new IntNode(unitsDigit, null);
                finalResultNode = multResult;
            }

            biggerNumberNode = biggerNumberNode.getNext();

        }
        if (extraPlus != 0) {
            multResult.setNext(new IntNode(extraPlus, null));
        }

        BigNumber multiplicationBigNumberResult = new BigNumber();
        multiplicationBigNumberResult.head = finalResultNode;

        return multiplicationBigNumberResult;
    }

    /**
     * the method multiply this big number with other big number
     * space complexity: O(n)
     * time complexity: O(n^2)

     * @param other: a big number to multiply with
     * @return big number which represent the multiplication result
     */
    public BigNumber multBigNumber(BigNumber other) {
        if (this.isZeroBigNumber(this) || this.isZeroBigNumber(other)) {
            // means that one of them is zero
            return new BigNumber();

        } else {
            int biggerNodeIndex = this.compareTo(other);

            BigNumber biggerNumber = this;
            BigNumber smallerNumber = other;

            if (biggerNodeIndex != 1) {
                biggerNumber = other;
                smallerNumber = this;
            }

            return multBigNumberAsNodes(smallerNumber.head, biggerNumber.head);
        }
    }

    /**
     * multiplying two big numbers represented as linked lists
     * space complexity: O(n)
     * time complexity: O(n^2)
     *
     * @param bigNumberNode: node represent the bigger number to multiply
     * @param smallNumberNode: node represent the smaller number to multiply
     * @return big number represent multiplication result
     */
    private BigNumber multBigNumberAsNodes(IntNode smallNumberNode, IntNode bigNumberNode) {

        // in order to not change the original big numbers by ref
        IntNode smallNumberDigit = smallNumberNode;
        IntNode biggerNumberNode = bigNumberNode;

        // being used for storing the zero(s) when summing the iterations (each iteration from the send one must be started with
        // one zero more that the previous iteration)
        IntNode contResultContainer = null;
        IntNode finalResultNode = null;

        BigNumber multiplicationResultBigNumber = new BigNumber();

        while (smallNumberDigit != null) {
            // iterating on smaller number digits

            BigNumber singleMultiBigNumberResult = this.multiplyNumberWithANode(finalResultNode, contResultContainer, smallNumberDigit,
                    biggerNumberNode);

            biggerNumberNode = bigNumberNode;

            multiplicationResultBigNumber = multiplicationResultBigNumber.addBigNumber(singleMultiBigNumberResult);

            if (contResultContainer != null) {
                contResultContainer.setNext(getZeroNode());
                contResultContainer = contResultContainer.getNext();
            } else {
                // thats the first time we multiply our digit with the other number and we want to move forward our
                // result container by keep pointer to the head
                contResultContainer = getZeroNode();
                finalResultNode = contResultContainer;

            }
            smallNumberDigit = smallNumberDigit.getNext();
        }
        return multiplicationResultBigNumber;
    }
}

package mmn11;


/**
 * the temperature class prints received temperature in different types or measurement.
 */
import java.util.Scanner;

public class Temperature
{
    /**
     * Gets temperature type and value
     * convert the value to the other two types
     * prints all types values
     */
    public static void main(String[] args)
    {
        final double FToCFactor = 5.0 / 9.0;
        final double KToFFactor = 9.0 / 5.0;
        final double CToKAddedValue = 273.15;
        final double KToFMinusValue = 273.15;
        final double KToFAddedValue = 32;
        final double FToCMinusValue = 32;

        Scanner scan = new Scanner(System.in);
        System.out.println("Please enter C, F or K (temperature type) ");

        char temperatureType = scan.next().toUpperCase().charAt(0);
        double temperatureValue = scan.nextDouble();

        double temperatureValueC = 0;
        double temperatureValueF = 0;
        double temperatureValueK = 0;

        switch(temperatureType)
        {
            case 'C':
                temperatureValueC = temperatureValue;
                temperatureValueK = temperatureValueC + CToKAddedValue;
                temperatureValueF = ((KToFFactor) * (temperatureValueK - KToFMinusValue)) + KToFAddedValue;
                break;
            case 'F':
                temperatureValueF = temperatureValue;
                temperatureValueC = (FToCFactor) * (temperatureValueF - FToCMinusValue);
                temperatureValueK = temperatureValueC + CToKAddedValue;
                break;
            case 'K':
                temperatureValueK = temperatureValue;
                temperatureValueF = ((KToFFactor) * (temperatureValueK - KToFMinusValue)) + KToFAddedValue;
                temperatureValueC = (FToCFactor) * (temperatureValueF - FToCMinusValue);
                break;
        }

        temperatureValueC = (double) Math.round(temperatureValueC * 100) / 100;
        temperatureValueF = (double) Math.round(temperatureValueF * 100) / 100;
        temperatureValueK = (double) Math.round(temperatureValueK * 100) / 100;

        System.out.println(temperatureValueC + " C");
        System.out.println(temperatureValueF + " F");
        System.out.println(temperatureValueK + " K");
    }
}

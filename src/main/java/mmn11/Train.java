package mmn11;

/**
 * The Train class prints the distance between two trains at the end of their driving.
 *
 */

import java.util.Scanner;

public class Train
{
    /**
     *  Gets the speed (KM/H) of two trains and the time they were driving,
     *  calculate the distance each train made,
     *  print the distance (KM) between the trains when they both stopped.
     */
    public static void main(String[] args)
    {
        final int MINUTES_IN_HOUR = 60;

        Scanner scan = new Scanner(System.in);

        System.out.println("Please enter 4 integers ");
        System.out.println("Please enter the speed of train 1:");
        int speedOfTrainOneKMH = scan.nextInt(); // KM per hour

        System.out.println("Please enter the time of train 1:");
        int timeOfTrainOneMinutes = scan.nextInt(); // in minutes

        System.out.println("Please enter the speed of train 2:");
        int speedOfTrainTwoKMH = scan.nextInt(); // KM per hour

        System.out.println("Please enter the time of train 2:");
        int timeOfTrainTwoMinutes = scan.nextInt(); // in minutes

        // Calculating the distance that train one did
        // First we'll transfer the time of train one to hours
        double timeOfTrainOneHours = timeOfTrainOneMinutes / (double) MINUTES_IN_HOUR;
        double trainOneDrivingDistance = timeOfTrainOneHours * speedOfTrainOneKMH;

        // Calculating the distance that train one did
        // First we'll transfer the time of train one to hours
        double timeOfTrainTwoHours = timeOfTrainTwoMinutes / (double) MINUTES_IN_HOUR;
        double trainTwoDrivingDistance = timeOfTrainTwoHours * speedOfTrainTwoKMH;

        // Calculating the distance between the trains
        double distanceBetweenTrains = Math.abs(trainOneDrivingDistance - trainTwoDrivingDistance);

        System.out.println("The distance between the trains is " + distanceBetweenTrains + " km");
    }
}

package mmn12;

import org.junit.Test;

public class DateTests {
    @Test
    public void openUTests() {
        // OPENU testers
        System.out.println("********** Test Date - Started **********");
        System.out.println("\n1. Testing Constructors and toString:");
        Date d1=new Date(3,5,1998);
        System.out.println("\td1:"+d1);
        Date d2=new Date(d1);
        System.out.println("\td2:"+d2);
        System.out.println("\n2. Testing accessors and mutators:");
        d1.setDay(8);
        d1.setMonth(10);
        d1.setYear(2016);
        System.out.println("\td1:"+d1);
        System.out.println("\tday of d1:"+d1.getDay());
        System.out.println("\tmonth of d1:"+d1.getMonth());
        System.out.println("\tyear of d1:"+d1.getYear());
        System.out.println("\n3. Testing equals method:");
        Date d3=new Date(3,5,1998);
        Date d4=new Date(3,5,1998);
        System.out.println("\td3:"+d3);
        System.out.println("\td4:"+d4);
        if(d3.equals(d4))
            System.out.println("\td3 is the same date as d4");
        else
            System.out.println("\td3 isn't the same date as d4");
        System.out.println("\n4. Testing before method:");
        if(d3.before(d1))
            System.out.println("\td3 is before d1");
        else
            System.out.println("\td3 isn't before d1");
        System.out.println("\n5. Testing after method:");
        if(d3.after(d1))
            System.out.println("\td3 is after d1");
        else
            System.out.println("\td3 isn't after d1");
        System.out.println("\n6. Testing difference method:");
        System.out.println("\tThe difference in days between dates d1 and d3 is : "+d1.difference(d3));
        System.out.println("\n7. Testing dayInWeek method:");
        Date d5=new Date(6,11,2016);
        System.out.println("\t" + d5+" occurs on : "+d5.dayInWeek());

        System.out.println("\n********** Test Date - Finished **********\n");

    }

    @Test
    public void extendedTests() {
        System.out.println("\n********** Edge Case Date Test - Start (MIGHT HAVE BUGS) **********\n");
        System.out.println("8. Testing invalid constructor: ");
        Date dYear1 = new Date(3,3,300);
        System.out.println("\tInvalid year (too small), therefore date is " + dYear1);
        Date dYear2 = new Date(3,3,10500);
        System.out.println("\tInvalid year (too big), therefore date is " + dYear2);
        Date dMonth1 = new Date(3,15,2018);
        System.out.println("\tInvalid month (too big), therefore date is " +dMonth1);
        Date dMonth2 = new Date(3,-3,2018);
        System.out.println("\tInvalid month (too small), therefore date is " +dMonth2);
        Date dDay1 = new Date(50,3,2017);
        System.out.println("\tInvalid day (too big), therefore date is " +dDay1);
        Date dDay2 = new Date(-7,3,2017);
        System.out.println("\tInvalid day (too small), therefore date is " +dDay2);
        Date dLeap = new Date(29, 2, 2016);
        System.out.println("\tInvalid day in February in leap year, therefore date is " +dLeap);

        System.out.println("\n9. Testing invalid setters: ");
        Date dSet = new Date(10,3,2017);
        System.out.println("  Original date  is " + dSet);
        dSet.setYear(300);
        System.out.println("\tInvalid year set (too small), therefore date remains " + dSet);
        dSet.setYear(10500);
        System.out.println("\tInvalid year set (too big), therefore date remains " + dSet);
        dSet.setMonth(33);
        System.out.println("\tInvalid month set (too big), therefore date remains " +dSet);
        dSet.setMonth(-4);
        System.out.println("\tInvalid month set (too small), therefore date remains " +dSet);
        dSet.setDay(44);
        System.out.println("\tInvalid day set (too big), therefore date remains " +dSet);
        dSet.setDay(-3);
        System.out.println("\tInvalid day set (too small), therefore date remains " +dSet);
        dSet.setYear(2016);
        dSet.setMonth(2);
        System.out.println("  Leap year date is " + dSet);
        dSet.setDay(29);
        System.out.println("\tInvalid day in February in leap year set, therefore date remains " +dSet);
    }
}

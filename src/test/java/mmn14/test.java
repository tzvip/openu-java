package mmn14;

import java.util.Scanner;

public class test {


    private static void print(int[][] a) {
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a[i].length; j++) {
                System.out.print(a[i][j] + "\t");
            }
            System.out.println();
        }
        System.out.println();

    }

    private static void printarr(int[] a) {
        for (int i = 0; i < a.length; i++) {

            System.out.print(a[i] + " , ");
            if (i % 17 == 15)
                System.out.println();

        }
        System.out.println();

    }

    public static void main(String[] args) {

        Ex14 a = new Ex14();
        int a1 = 0;

        System.out.println("enther number of method to check:\n 1:what\n 2:test\n 3:subStrC and subStrMaxC\n 4:spiderman and spidermanPhoneBooth20\n 5:countPaths\n");
        Scanner z = new Scanner(System.in);
        int v = z.nextInt();

        if (v == 1) {
            System.out.println("****************--------method 1:-------****************\n");
            System.out.println("*********************************************************\n");

            System.out.println("*********  what *********\n");

            int[][] arr1 = new int[][]{
                    {0, 0, 1, 2, 2, 21, 23, 23,},
                    {1, 5, 7, 7, 7, 21, 23, 23,},
                    {41, 47, 52, 53, 53, 65, 72, 73,},
                    {41, 51, 56, 57, 58, 99, 99, 99,},
                    {41, 52, 60, 61, 70, 100, 112, 178,},
                    {42, 52, 98, 100, 111, 147, 150, 180,},
                    {42, 52, 112, 112, 123, 154, 172, 185,},
                    {78, 82, 121, 134, 136, 174, 182, 199,},
            };
            int[][] arr03 = new int[][]{
                    {41, 41, 41, 41, 41, 52, 60, 60, 60, 60,},
                    {41, 41, 41, 41, 41, 53, 60, 60, 60, 60,},
                    {41, 41, 41, 41, 41, 57, 60, 60, 60, 60,},
                    {41, 41, 41, 41, 41, 60, 60, 60, 60, 60,},
                    {41, 41, 41, 41, 41, 100, 129, 131, 131, 131,},
                    {41, 41, 41, 41, 41, 147, 150, 180, 180, 199,},
                    {41, 52, 112, 112, 123, 154, 172, 180, 180, 199,},
                    {41, 82, 121, 134, 136, 174, 182, 199, 199, 199,},
                    {41, 82, 121, 134, 136, 174, 182, 199, 199, 199,},
                    {41, 82, 121, 134, 136, 174, 182, 199, 199, 199,},
            };
            int[][] arr04 = new int[][]{
                    {41, 41, 41, 41, 41, 41, 41, 41, 41, 41,},
                    {41, 41, 41, 41, 41, 41, 41, 41, 41, 41,},
                    {41, 41, 41, 41, 41, 41, 41, 41, 41, 41,},
                    {41, 41, 41, 41, 41, 41, 41, 41, 41, 41,},
                    {41, 41, 41, 41, 41, 100, 129, 131, 180, 199,},
                    {41, 41, 41, 41, 41, 147, 150, 180, 180, 199,},
                    {41, 52, 112, 112, 123, 154, 172, 180, 180, 199,},
                    {41, 82, 121, 134, 136, 174, 180, 199, 199, 199,},
                    {82, 82, 134, 136, 174, 182, 199, 199, 199, 199,},
                    {199, 199, 199, 199, 199, 199, 199, 199, 199, 199,},
            };

            int arr2[] = new int[]
                    {0, 1, 2, 5, 7, 21, 23, 41, 42, 47, 51, 52, 53, 56, 57, 58, 60, 61, 65, 70, 72, 73, 78, 82, 98, 99, 100, 111, 112, 121, 123, 134, 136, 147, 150, 154, 172, 174, 178, 180, 182, 185, 199};

            System.out.println("array:\n");
            print(arr1);
            System.out.println("\n you need to find this numbers:\n");
            printarr(arr2);

            System.out.println("\n check to find 0:");
            if (a.findValWhat(arr1, 0))
                System.out.println("found: " + "0\n ");
            else System.out.println("not found: " + "0\n ");

            System.out.println("you need to find 43 numbers\n ");
            String g = new String("  ");

            for (int m = 0, n = 1; m < 201; m++) {
                if (a.findValWhat(arr1, m)) {
                    System.out.print("  " + n + ": expected: " + arr2[n - 1] + ". you found: " + m + "." + g + "\t###\t  ");
                    n++;
                    g = new String("");
                    if (n % 2 == 1)
                        System.out.println("\n");
                    if (n < 7) g = new String("  ");
                }
            }
            System.out.println("\n\n********** method 'What' - End test **********");
        }

        if (v == 2) {
            System.out.println("\n\n*********  test *********\n");
            int[][] arr3 = new int[][]{
                    {0, 2, 1, 2, 0, 5, 5, 5,},
                    {21, 21, 7, 7, 7, 21, 21, 21,},
                    {21, 21, 21, 21, 21, 21, 21, 21,},
                    {21, 21, 23, 42, 41, 23, 21, 21,},
                    {60, 56, 57, 58, 53, 52, 47, 51,},
                    {61, 65, 70, 72, 73, 78, 82, 98,},
                    {112, 121, 112, 134, 123, 100, 98, 111,},
                    {136, 136, 136, 134, 147, 150, 154, 134,},
            };
            int[][] arr5 = new int[][]{
                    {41, 41, 41, 41, 41, 52, 60, 60, 60, 60,},
                    {60, 60, 60, 60, 60, 60, 60, 60, 60, 60,},
                    {60, 60, 60, 60, 60, 60, 60, 60, 60, 60,},
                    {60, 60, 60, 60, 60, 60, 60, 60, 60, 60,},
                    {60, 60, 60, 60, 60, 62, 64, 60, 60, 60,},
                    {110, 110, 65, 103, 107, 110, 110, 110, 110, 110,},
                    {199, 199, 112, 112, 199, 154, 172, 180, 199, 199,},
                    {199, 199, 199, 199, 199, 199, 199, 199, 199, 199,},
                    {199, 199, 199, 199, 199, 199, 199, 199, 199, 199,},
                    {215, 199, 199, 199, 212, 220, 201, 199, 220, 199,},
            };

            int arr4[] = new int[]
                    {0, 1, 2, 5, 7, 21, 23, 41, 42, 47, 51, 52, 53, 56, 57, 58, 60, 61, 65, 70, 72, 73, 78, 82, 98, 100, 111, 112, 121, 123, 134, 136, 147, 150, 154};

            System.out.println("array:\n");
            print(arr3);
            System.out.println("\n you need to find this numbers:\n");
            printarr(arr4);
            System.out.println("\n check to find 0:");
            if (a.findValWhat(arr3, 0))
                System.out.println("found: " + "0\n ");
            else System.out.println("not found: " + "0\n ");

            System.out.println("you need to find 35 numbers:\n ");
            String g = new String("  ");
            for (int m = 0, n = 1; m < 217; m++) {
                if (a.findValTest(arr3, m)) {
                    System.out.print("  " + n + ": expected: " + arr4[n - 1] + ". you found: " + m + "." + g + "\t###\t  ");
                    n++;
                    g = new String("");
                    if (n % 2 == 1)
                        System.out.println("\n");
                    if (n < 7) g = new String("  ");
                }
            }
            System.out.println("\n\n********** method 'Test' - End test **********");
        }

        if(v==3){
            System.out.println("\n****************--------method 2:-------****************\n");
            System.out.println("*********************************************************\n");

            System.out.println("string s1: sdvfsdfdsfdsvfffssf (7f, 1v).\nstring s2: ffffffff (8f).\nstring s3: fsdfsdfxxdsfds (4s, 4f,2x).\nstring s4: fsdsdcrtzjjjysf (3j).\n");
            String s1= new String ("sdvfsdfdsfdsvfffssf"); //  7 f , 1 v
            String s2= new String ("ffffffff");       // 8 f
            String s3= new String ("fsdfsdfxxdsfds");  //4 s ,4f
            String s4= new String ("fsdsdcrtzjjjysf"); //3 j

            System.out.println("\n Testing subStrC:\n");
            a1=a.subStrC(s1 ,  'f');
            System.out.println("\t test:(s1,f). expected:5. you have:"+a1);
            a1=a.subStrC(s1 ,  'v');
            System.out.println("\t test:(s1,v). expected:0. you have:"+a1);
            a1=a.subStrC(s2 ,  'f');
            System.out.println("\t test:(s2,f). expected:6. you have:"+a1);
            a1=a.subStrC(s2 ,  's');
            System.out.println("\t test:(s2,s). expected:0. you have:"+a1);
            a1=a.subStrC(s3 ,  's');
            System.out.println("\t test:(s3,s). expected:2. you have:"+a1);
            a1=a.subStrC(s3 ,  'x');
            System.out.println("\t test:(s3,x). expected:0. you have:"+a1);
            a1=a.subStrC(s4 ,  'j');
            System.out.println("\t test:(s4,j). expected:1. you have:"+a1);
            a1=a.subStrC(s4 ,  'z');
            System.out.println("\t test:(s4,z). expected:0. you have:"+a1);

            System.out.println("************************");
            System.out.println("\n. Testing subStrMaxC:");

            a1=a.subStrMaxC(s1 ,  'f' , 13);
            System.out.println("\t test:(s1,f,13). expected:21.  you have:"+a1);
            a1=a.subStrMaxC(s1 ,  'f' , 0);
            System.out.println("\t test:(s1,f,0).  expected:6.   you have:"+a1);
            a1=a.subStrMaxC(s1 ,  'f' , 8);
            System.out.println("\t test:(s1,f,8).  expected:21.  you have:"+a1);
            a1=a.subStrMaxC(s1 ,  'f' , 3);
            System.out.println("\t test:(s1,f,3).  expected:18.  you have:"+a1);
            a1=a.subStrMaxC(s1 ,  'f' , 1);
            System.out.println("\t test:(s1,f,1).  expected:11.  you have:"+a1);
            a1=a.subStrMaxC(s1 ,  'f' ,  5);
            System.out.println("\t test:(s1,f,5).  expected:21.  you have:"+a1);
            a1=a.subStrMaxC(s2 ,  'f' , 13);
            System.out.println("\t test:(s2,f,13). expected:28.  you have:"+a1);
            a1=a.subStrMaxC(s3 ,  'x' , 2);
            System.out.println("\t test:(s3,x, 2). expected:1.   you have:"+a1);
            a1=a.subStrMaxC(s3 ,  'f' , 13);
            System.out.println("\t test:(s3,f,13). expected:6.   you have:"+a1);
            a1=a.subStrMaxC(s4 ,  'z' , 13);
            System.out.println("\t test:(s4,z,13). expected:0.   you have:"+a1);

            System.out.println("\n********** method 2 - End test **********");
        }
        if(v==4){
            System.out.println("****************--------method 3:-------****************\n");
            System.out.println("*********************************************************\n");

            System.out.println("\n Testing spiderman:\n");

            a1=a.spiderman(0);
            System.out.println("check 0:\t expected:0.         you have:"+a1);
            a1=a.spiderman(1);
            System.out.println("check 1:\t expected:1.         you have:"+a1);
            a1=a.spiderman(2);
            System.out.println("check 2:\t expected:2.         you have:"+a1);
            a1=a.spiderman(3);
            System.out.println("check 3:\t expected:3.         you have:"+a1);
            a1=a.spiderman(4);
            System.out.println("check 4:\t expected:5.         you have:"+a1);
            a1=a.spiderman(11);
            System.out.println("check 11:\t expected:144.      you have:"+a1);
            a1=a.spiderman(20);
            System.out.println("check 20:\t expected:10946.    you have:"+a1);
            a1=a.spiderman(25);
            System.out.println("check 25:\t expected:121393.   you have:"+a1);

            System.out.println("************************\n");
            System.out.println("\n Testing spidermanPhoneBooth20:\n");

            a1=a.spidermanPhoneBooth20 (3);
            System.out.println("check 3:\t expected:0.         you have:"+a1);
            a1=a.spidermanPhoneBooth20 (19);
            System.out.println("check 19:\t expected:0.        you have:"+a1);
            a1=a.spidermanPhoneBooth20 (20);
            System.out.println("check 20:\t expected:10946.    you have:"+a1);
            a1=a.spidermanPhoneBooth20 (21);
            System.out.println("check 21:\t expected:17711.    you have:"+a1);
            a1=a.spidermanPhoneBooth20 (22);
            System.out.println("check 22:\t expected:17711.    you have:"+a1);
            a1=a.spidermanPhoneBooth20 (23);
            System.out.println("check 23:\t expected:24476.    you have:"+a1);
            a1=a.spidermanPhoneBooth20 (27);
            System.out.println("check 27:\t expected:98891.    you have:"+a1);
            a1=a.spidermanPhoneBooth20 (30);
            System.out.println("check 30:\t expected:0.        you have:"+a1);

            System.out.println("\n\n********** method 3 - End test **********");
        }

        if(v==5){
            System.out.println("****************--------method 4:-------****************\n");
            System.out.println("*********************************************************\n");

            int [][] array0 = new int [8][8];
            for(int i=0; i<8; i++) {
                for(int j=0; j<8; j++) {
                    array0[i][j]=11;
                }
            }

            int [][] array1 = new int [6][8];
            for(int i=0; i<6; i++) {
                for(int j=0; j<8; j++) {
                    array1[i][j]=11;
                }
            }
            int [][] array6 = new int [][]{
                    { 22, 0,    0,  54,},
                    { 0,  0,    0,  20,},
                    { 0,  10,  10,  10,},
                    { 25, 10,  10 , 10,},
                    { 0 , 10,  10,  10,},
                    { 20, 1,   10 , 45,}
            };




            int [][] array2 = new int [][]{
                    { 12, 22, 23, 54,},
                    { 43, 35, 21, 20,},
                    { 34, 21, 43, 21,},
                    { 25, 30, 0 , 20,},
                    { 0 , 22, 10, 10,},
                    { 20, 13, 3 , 45,}
            };

            int [][] array3 = new int [][]{
                    { 12, 22, 23, 54,},
                    { 43, 35, 21, 20,},
                    { 34, 21, 43, 21,},
                    { 25, 30, 0 , 20 ,},
                    { 0 , 22, 0 , 10,},
                    { 20, 13, 3 , 45,}
            };

            int [][] array4 = new int [][]{
                    { 12, 0,  23, 54, 23, 54, 23, 54,},
                    { 11, 35, 21, 20, 23, 54, 23, 54,},
                    { 11, 11, 11, 11, 23, 11, 11, 11,},
                    { 11, 11, 11 , 11, 11, 11, 11,11,},
                    { 0 , 22, 10, 10, 13, 11, 11, 11,},
                    { 20,13, 11 , 11, 11, 11, 11, 0,}
            };



            int [][] array5 = new int [][]{
                    { 12, 0,  23, 54, 23, 54, 23, 54,},
                    { 11, 35, 21, 20, 23, 54, 23, 54,},
                    { 11, 11, 11, 11, 23, 11, 11, 11,},
                    { 11, 11, 11 , 11, 11, 11, 11,11,},
                    { 0 , 22, 10, 10, 13, 11, 11, 11,},
                    { 20,13, 11 , 11, 11, 11, 11, 11,}
            };

            System.out.println("\narray 0:\n");
            print(array0);
            System.out.println("\narray 1:\n");
            print(array1);
            System.out.println("\narray 2:\n");
            print(array2);
            System.out.println("\narray 3:\n");
            print(array3);
            System.out.println("\narray 4:\n");
            print(array4);
            System.out.println("\narray 5:\n");
            print(array5);
            System.out.println("\narray 6:\n");
            print(array6);

            System.out.println("\n\n check array 0:");
            a1=a.countPaths(array0);
            System.out.println("\n expected:1. you have:"+a1);
            System.out.println("\n check array 1:");
            a1=a.countPaths(array1);
            System.out.println("\n expected:0. you have:"+a1);
            System.out.println("\n check array  2: (with mmn14 example)");
            a1=a.countPaths(array2);
            System.out.println("\n expected:3. you have:"+a1);
            System.out.println("\n check array 3: (with one more inside cell contain 0)");
            a1=a.countPaths(array3);
            System.out.println("\n expected:2. you have:"+a1);
            System.out.println("\n check array 4 ");
            a1=a.countPaths(array4);
            System.out.println("\n expected:2. you have:"+a1);
            System.out.println("\n check array 5: ( same as array 4 exept final cell)");
            a1=a.countPaths(array5);
            System.out.println("\n expected:2. you have:"+a1);
            System.out.println("\n check array 6 ");
            a1=a.countPaths(array6);
            System.out.println("\n expected:4. you have:"+a1);
            System.out.println("\n\n********** method 4 - End test **********");
        }
    }
}
//
//

//
//
//
//    }
//
//

package mmn13;

public class LibraryTests {
    public static void main(String[] args) {
        Book b1 = new Book("Pride and Prejudice" , "Jane Austen" , 1813 , 350);
        Book b2 = new Book("Harry Potter and the sorcerer's stone" , "J.K.Rowling" , 1997 , 309);
        Book b3 = new Book("Pride and Prejudice" , "Jane Austen" , 1813 , 350);
        Book b4 = new Book("Harry Potter and the sorcerer's stone" , "J.K.Rowling" , 1997 ,309 );
        Book b5 = new Book("The sun is also a star" , "Nicola Yoon" , 2016 , 340);

        Library lib = new Library();
        if (lib.addBook(b1)) {
            System.out.println(" OK to add a book");
        } else {
            System.out.println(" Fail to add a book - fix this method befor continue ...");
            return;
        }
        lib.addBook(b2);
        lib.addBook(b3);
        lib.addBook(b4);
        lib.addBook(b5);
        System.out.println("\n try toString:\n" + lib);

        int bor = lib.howManyBooksBorrowed();
        System.out.println("\n num of borrowed book = " + bor + "  expected 0");

        Book tmp = lib.remove("Pride and Prejudice");
        tmp.borrowBook("Yossi", new Date(1, 1, 2018));
        System.out.println("\n after remove first \"Pride and Prejudice\" copy - the library is: (expected 4 books)\n" + lib);

        lib.addBook(tmp);
        tmp = lib.remove("Harry Potter and the sorcerer's stone");
        tmp.borrowBook("Yossi", new Date(1, 1, 2018));
        lib.addBook(tmp);

        bor = lib.howManyBooksBorrowed();
        System.out.println("\n num of borrowed after Yossi borrowed 2 books = " + bor + "  expected 2");

        bor = lib.howManyBooksBorrowedToStudent("Yossi");
        System.out.println("\n howManyBooksBorrowedToStudent Yossi =" + bor + "  expected=" + 2);

        lib.addBook(b1);
        String pop = lib.mostPopularAuthor();
        System.out.println("\n mostPopularAuthor=" + pop + " expected=Jane Austen");

        Book old = lib.oldestBook();
        System.out.println("\n oldesBook = " + old + "  expected= Pride and Prejudice");

        lib.removeBook(b1);
        System.out.println("\n after remove all \"Pride and Prejudice\" books - the library should contained 3 books:\n" + lib);
    }
}
